# arXiv Parser

Small script that take a list of arxiv.org links
and then extract the following information:

 * Title
 * Authors
 * Submission date
 * Summary

## How to use it

`python arxiv_parser.py sample_links`
