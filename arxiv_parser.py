#!/usr/bin/env python

"""arxiv_parser.py: arXiv single publication website parser"""
__author__ = "Cristián Maureira-Fredes"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "cmaureirafredes@gmail.com"

from bs4 import BeautifulSoup
import urllib.request
import sys

def print_wiki_format(entry):
    print("""
^ %s ^^
| Authors | %s |
| URL | %s |
| %s ||
| %s |
|||
    """  % (entry['title'], entry['authors'], entry['url'], entry['summary'], entry['date']))

def print_raw_format(entry):
    print("""
Title: %s
Authors: %s
URL: %s
Date: %s
%s
    """  % (entry['title'], entry['authors'], entry['url'], entry['summary'], entry['date']))

def get_data(url):
    d = {}
    response = urllib.request.urlopen(url).read()

    soup = BeautifulSoup(response, "lxml")
    title = soup.findAll("h1", {"class" : "title mathjax"})
    authors = soup.findAll("div", {"class" : "authors" })
    submitted = soup.findAll("div", {"class" : "dateline" })
    summary = soup.findAll("blockquote", {"class" : "abstract mathjax" })

    d['title'] = title[0]
    d['authors'] = authors[0]
    d['date'] = submitted[0]
    d['summary'] = summary[0]

    for i in d:
        tmp = d[i]
        tmp = tmp.get_text() # Getting text frm BS
        if i != "date":
            tmp = tmp.split(":")[1:] # Removing description of the text
        tmp = "".join(tmp) # List to string
        tmp = tmp.replace("\n"," ") # Removing newlines
        tmp = tmp.strip() # Striping string
        d[i] = tmp

    d['url'] = url

    return d

def get_urls_from_file(fname):
    p = []
    try:
        f = open(fname, "r")
    except:
        print("File doesn't exist")

    return [i.strip() for i in f.readlines() if i.strip() != ""]


if __name__ == "__main__":
    urls = get_urls_from_file(sys.argv[1])
    for url in urls:
        d = get_data(url)
        #print_wiki_format(d)
        print_raw_format(d)
